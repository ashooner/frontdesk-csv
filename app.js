const MongoClient = require('mongodb').MongoClient
const express = require('express')
const app = express()
var json2csv = require('json2csv');


app.get('/', function (req, res) {
    //res.send('Hello World!')

    MongoClient.connect('mongodb://127.0.0.1:3001/meteor', function(err, db) {

        var collection = db.collection('timelogs');
        collection.findOne({}, function(err, item) {
            res.send(item);
        });
    });

})

app.get('/timelogs', function (req, res) {
    //res.send('Hello World!')

    MongoClient.connect('mongodb://127.0.0.1:3001/meteor', function(err, db) {
        var collection = db.collection('timelogs');
        collection.aggregate([
            { "$group": {
                "_id": {
                    "year": { "$year": "$startTime" },
                    //"month":{ "$month": "$startTime" }
                },
                "count": { "$sum": 1 }
            }}
        ], function(err, docs){

            var list = "";

            for (var i = 0, len = docs.length; i < len; i++) {
                list+='<li><a href="/timelogs/'+docs[i]._id.year+'/">'+docs[i]._id.year+'</a></li>';
            }

            res.send('' +
                '<h1>Timelogs by Year</h1>' +
                '<ul>' +
                list +
                '</ul>')
        })


    })

});

app.get('/timelogs/:year', function (req, res) {

        var selectedYear = new Date(req.params.year);
        var nextYear = new Date(req.params.year+1);

    console.log(selectedYear, nextYear);
        var connectCallback = function(err, db){
            var collection = db.collection('timelogs');
            collection.aggregate([
                {
                    "$match":{
                        "startTime" : {
                            //$lt: addMonths(selectedMonth, 1), //the next month
                            $lt:nextYear,
                            $gte:selectedYear //this year and month
                        }
                    }
                },
                {
                    "$group": {
                        "_id": {
                            "month": { "$month": "$startTime" }
                        },
                        "count": {
                            "$sum": 1
                        }
                    }
                }

            ],aggregateCallback)
        }

        var aggregateCallback = function(err, docs){
            var list = "";
            console.log(docs);
            for (var i = 0, len = docs.length; i < len; i++) {

                var date = new Date(req.params.year, docs[i]._id.month),
                    locale = "en-us",
                    month = date.toLocaleString(locale, { month: "long" });

                list+='<li><a href="/timelogs/'+req.params.year+'/'+docs[i]._id.month+'/">'+month+'</a></li>';
            }

            res.send('' +
                '<h1>'+req.params.year+' Timelogs by Month</h1>' +
                    '<p>(csv downloads)</p>'+
                '<ul>' +
                list +
                '</ul>')

        };

    MongoClient.connect('mongodb://127.0.0.1:3001/meteor', connectCallback)
});


app.get('/timelogs/:year/:month', function (req, res) {
    //res.send('Hello World!')

    var selectedMonth = new Date(req.params.year,req.params.month-1);
    var nextMonth = new Date(req.params.year,req.params.month);
    var theData;
    MongoClient.connect('mongodb://127.0.0.1:3001/meteor', function(err, db) {

        var collection = db.collection('timelogs');
        collection.find({
            "startTime" : {
               //$lt: addMonths(selectedMonth, 1), //the next month
                $lt:nextMonth,
                $gte: selectedMonth //this year and month
            }
        }).toArray(function(err, docs){
           // console.log(docs);
            theData = docs;

            var columns = [
                {
                    value: 'person.firstName',
                    label: 'First Name'
                }, {
                    value: 'person.lastName',
                    label: 'Last Name'
                },
                {
                    value: 'activityType',
                    label:'Activity'
                },{
                    value:'startTime',
                    label:'Start'
                },{
                    value:'endTime',
                    label:'End'
                }, {
                    value:'totalTime',
                    label:'Total'
                }
            ];

            json2csv({ data: theData, fields: columns, doubleQuotes:""}, function(err, csv) {
                if (err) {
                    console.log(err);
                    res.statusCode = 500;
                    return res.end(err.message);
                }
                res.attachment('timelogs_'+req.params.year + '_'+req.params.month+'.csv');
                res.end(csv);
            });
        })

    });



})


app.listen(9000, function () {
    console.log('Example app listening on port 9000!')
});

